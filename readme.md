## Инструкция по запуску тестов 

### Общие настройки среды
1. Установить Java 8. Проверить, что **java -version** возвращает версию 1.8.

### Запуск тестов локально из консоли
1. В папке проекта запустить тесты командой
gradle clean build test --info
2. Генерация allure репорта командой
gradle allureAggregatedReport -i
3. Allure репорт сгенерируется в allure/report в корневой папке проекта. Отчет можно посмотреть нажав правой кнопкой мыши по index.html и выбрать браузер.

### Запуск тестов в Docker (среда Windows)
#### 1. Установка Docker
1. Проверить наличие аппаратной виртуализации утилитой havdetectiontool.exe по ссылке: [https://www.microsoft.com/en-us/download/details.aspx?id=592](https://www.microsoft.com/en-us/download/details.aspx?id=592)
2. Скачать VirtualBox (подойдет, например, версия 5.1.22): [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
3. Установить VirtualBox. По ходу процесса соглашаться на предлагаемую установку продуктов Oracle (Network Drivers и др.)
4. Установить Docker Toolbox. Для этого скачать и запустить (как администратор) DockerToolbox-17.05.0-ce.exe (проверял на данной версии). Ссылка: [https://github.com/docker/toolbox/releases/tag/v17.05.0-ce](https://github.com/docker/toolbox/releases/tag/v17.05.0-ce) При установке убедиться, что отмечено "Установить Git for Windows" (если Git еще не установлен). Без Git при запуске Docker может возникать ошибка "Windows is searching for bash.exe".
5. Запустить Docker Quickstart Terminal (Пуск -> All programs). Если появится ошибка "Error getting ip address" - зайти в VirtualBox и удалить виртуальную машину default, после чего снова попробовать запустить Docker (машина default при этом будет создана автоматически).
6. Все последующие команды Docker выполнять в Docker Quickstart Terminal (а не в стандартной cmd). Для проверки работы Docker выполнить команду: ****docker run hello-world****. При появлении ошибки "x509 certificate signed by unknown authority" повторить ту же самую команду, но с дополнительной опцией: ****docker run hello-world --insecure-registry****. Признаком успешного запуска является сообщение "Hello from Docker!"
7. После первого запуска Docker Quickstart Terminal выполнить (неважно, в bash или cmd) команды: ```docker-machine start default``` и ```docker-machine env default```. Последняя команда выводит подсказку, как установить переменные окружения для конкретного терминала. Значения, выводимые этой командой могут не соответствовать актуальным значениям переменных (это просто примеры).

#### 2. Настройка Docker
1. Для доступа к банковскому репозиторию docker образов необходимо прописать следующие настройки в конфигурационном файле daemon.json:
"insecure-registries": ["seis.rccf.ru:8415"]

#### 3. Скачать Docker образ 
docker login seis.rccf.ru:8415 -u $NEXUS2_USER -p $NEXUS2_PASS
docker pull seis.rccf.ru:8415/arsnova/autotests/walk_through_tests:stable

#### 4. Запуск тестов в Docker локально
Для логина в docker репо необходима сервисная учетка svc_jenkins
В папке проекта:
docker build -t autotest .
docker run --rm --name tests --network host autotest

### Запуск тестов в IntelliJ
В каждом модуле с тестами есть Scenario Runner, который позволяет запустить тесты данного модуля.
