import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SimpleTest {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.google.com/");
        driver.findElement(By.cssSelector("input[name='q']")).sendKeys("Gitlab ", Keys.ENTER);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until((WebDriver d) -> d.findElement(By.xpath("//h3[contains(text(), 'GitLab')]")));
        wait.until((WebDriver d) -> d.findElement(By.xpath("//h3[contains(text(), 'GitLab')]")));

        driver.quit();
    }
}
